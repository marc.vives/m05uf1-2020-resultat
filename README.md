Objectiu, copiar un repositori amb totes les seves branques i commits, fer algunes modificacions i commits/merge i pujar-ho tot a un repositori propi de l'alumne.

- El repositori a copiar és: https://gitlab.com/marc.vives/m05-uf1-gitexam.git
- El teu projecte s'ha de dir M05UF1-2020. Recorda revisar que tens totes les branques.

- Crea una branca anomenada *grup* a partir del codi de la *master*
  En aquesta branca, edita el codi de *grup()* així:
```
  public static String grup(){
    return "grupA";
  }   
  ```
- Incorpora els canvis de la branca *modul*, a la branca *master*
- Incorpora els canvis de la branca *grup*, a la branca *master*

- Puja totes les branques, commits.... (primer puja master)